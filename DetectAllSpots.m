function handles = DetectAllSpots(handles)

% Help for the Detect All Spots module:
% Category: Object Processing
%
% SHORT DESCRIPTION:
% Simplified spot detection function based on A-Trous wavelet transform and
% Hough transformation... 
% TODO: write this once the algorithm is stable and clean.
% *************************************************************************
%
% This module detect spots on the image based on the A-Trous wavelet
% transform.
%
% Settings:
%
% See also Identify primary Identify Secondary modules.

% CellProfiler is distributed under the GNU General Public License.
% See the accompanying file LICENSE for details.
%
% Developed by Peter Horvath 2010 and Abel Szkalisity 2016.

% $Revision: 5025 $

%%%%%%%%%%%%%%%%%
%%% VARIABLES %%%
%%%%%%%%%%%%%%%%%
%drawnow

[~, CurrentModuleNum, ModuleName] = CPwhichmodule(handles);

%textVAR01 = What did you call the images you want to process?
%infotypeVAR01 = imagegroup
ImageName = char(handles.Settings.VariableValues{CurrentModuleNum,1});
%inputtypeVAR01 = popupmenu

%textVAR02 = What do you want to call the spots identified by this module?
%defaultVAR02 = Spots
%infotypeVAR02 = objectgroup indep
SpotObjectName = char(handles.Settings.VariableValues{CurrentModuleNum,2});

%textVAR03 = What is the radius of the smallest droplet that you want to detect?
%defaultVAR03 = 0
minSpotRadius = str2double(handles.Settings.VariableValues{CurrentModuleNum,3});
minSpotSize = minSpotRadius.^2 *pi;

%textVAR04 = What is the radius of the largest droplet that you want to detect?
%defaultVAR04 = 40
maxSpotRadius = str2double(handles.Settings.VariableValues{CurrentModuleNum,4});
maxSpotSize = maxSpotRadius.^2 *pi;

%textVAR05 = Single layer detection?
%defaultVAR05 = No
%choiceVAR05 = No
%choiceVAR05 = Yes
%inputtypeVAR05 = popupmenu
singleLayerDetection = char(handles.Settings.VariableValues{CurrentModuleNum,5});

%textVAR06 = Noise removal factor (removes background + n times std).
%defaultVAR06 = 1.5
NoiseRemovalFactor = str2double(handles.Settings.VariableValues{CurrentModuleNum,6});

%textVAR07 = Static threshold for spot identificaiton.
%defaultVAR07 = 1e-010
FinalSpotThreshold = str2double(handles.Settings.VariableValues{CurrentModuleNum,7});

%textVAR08 = Threshold for spot extension detection (Automatic, Otsu or number between 0-1)
%defaultVAR08 = Automatic
AreaDetectionThreshold = char(handles.Settings.VariableValues{CurrentModuleNum,8});

%textVAR09 = Define a circularity threshold for droplets
%defaultVAR09 = 0.8
circularityThreshold = str2double(handles.Settings.VariableValues{CurrentModuleNum,9});

%textVAR10 = Specify a name for the outlines of the identified objects! (optional)
%defaultVAR10 = Do not save
%infotypeVAR10 = outlinegroup indep
SaveOutlines = char(handles.Settings.VariableValues{CurrentModuleNum,10});

%%%VariableRevisionNumber = 3

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PRELIMINARY CALCULATIONS & FILE HANDLING %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%drawnow

tic

if strcmp(singleLayerDetection,'Yes')
    singleLayerBool = 1;
else
    singleLayerBool = 0;
end

%%% Reads (opens) the image you want to analyze and assigns it to a
%%% variable.
OrigImage = CPretrieveimage(handles,ImageName,ModuleName,'MustBeGray','CheckScale');

[xs, ys] = size(OrigImage);
origMean = mean(OrigImage(:));
origStd = std(OrigImage(:));    
    
imFilterSize = 5;
if singleLayerBool
    largestWaveletSize = 1; %Only the first layer
else
    largestWaveletSize= 4; % It is empricially determined that the 5 level wavelet responds to quite big circles with radius ~15-16. another reason for choosing 5 as an upper limit is that greater values cause very large a-trous filter size which means that completely flat regions are also affected by far away droplets. (it is for as we have 4 separate level from 2 to 5)
end

%waveletMaxSpotSize = min(maxSpotSize,largestWaveletSize);
%maxLevelCalc = (waveletMaxSpotSize-imFilterSize)/(imFilterSize-1);%this is the size of the gap
%maxLevelCalc = ceil(sqrt(maxLevelCalc)); %this is the input level size

%empricially determined wavelet response limits for size. Entry indices are
%the wavelet scales-1
sizeLimits = [41,385,787,1055];

% objects with circularity below the threshold are removed.
%the circularity can go above 1 becuase the pixel space is discrete

w = a_trous(-OrigImage, largestWaveletSize+1,imFilterSize);

FinalLabelMatrixImage  = zeros(size(OrigImage));
identifiedRegionpropsPerLevel = cell(largestWaveletSize,4); % store area and perimeter next to it

%mask for shrinking
sr = strel('disk',1);

%Thresholded image for droplets area detection
%keepThreshold = 0.5;
%keepThreshold = 0.85;
%threshImage = thresholdPercentile(OrigImage,keepThreshold);

if ~ (strcmp(AreaDetectionThreshold,'Automatic') || strcmp(AreaDetectionThreshold,'Otsu') )
    level = str2double(AreaDetectionThreshold);    
else
    level = graythresh(OrigImage);
end

threshImage = im2bw(OrigImage,level);

for aTrousLevel = 1:largestWaveletSize
    
    %This will store the center points for the current scale level
    CurrentLabelMatrixImage = zeros(size(FinalLabelMatrixImage));
        
    product = w(:,:,aTrousLevel);

    % coarse threshold
    spotthres = mean(product(:)) + NoiseRemovalFactor*std(product(:));   
    product(product < spotthres) = 0;

    SizeOfSmoothingFilter = aTrousLevel;
    
    % shape based enhancement
    filter = fspecial('disk', SizeOfSmoothingFilter);
    product = imfilter(product, filter);
    %intensity based enhancement
    filter = fspecial('gaussian', SizeOfSmoothingFilter);
    product = imfilter(product, filter);
    
    %{
    identifiedRegionLabels = product;
    identifiedRegionLabels(product>0) = 1;
    CC = bwconncomp(identifiedRegionLabels);
    for i=1:CC.NumObjects
        identifiedRegionLabels(CC.PixelIdxList{i}) = i;
    end
    %}

    %[~,IMAX,~,~] = extrema2(product);
    IMAX = find(imregionalmax(product));
    %REMOVE static threshold    
    IMAX = IMAX(product(IMAX) > FinalSpotThreshold);
    

    %% cleaning

    % this step could be improved by index trasform
    mask = zeros(size(OrigImage));
    mask(IMAX) = 1;
    [r c] = find(mask == 1);

    % delete spots on the boundary
    boundary = 10;
    todel = find(c<boundary); r(todel) = []; c(todel) = [];
    todel = find(r<boundary); r(todel) = []; c(todel) = [];
    todel = find(c>ys-boundary); r(todel) = []; c(todel) = [];
    todel = find(r>xs-boundary); r(todel) = []; c(todel) = [];


    %% remove spots too close to one another
    minDist = 2^aTrousLevel;
    minDistS = minDist^2;

    todel = [];

    filter = fspecial('disk', 3);

    avgint = imfilter(OrigImage, filter, 'replicate');

    for i=1:length(c)
        for j=i+1:length(c)
            dist = (r(i) - r(j))^2 + (c(i) - c(j))^2;
            if dist < minDistS
                if (avgint(r(i), c(i)) < avgint(r(j), c(j)))
                    todel = [todel i];
                else
                    todel = [todel j];                
                end;
            end;
        end;
    end;

    r(todel) = []; c(todel) = [];
    

    for i=1:length(r)
        CurrentLabelMatrixImage(r(i), c(i)) = i;
    end
    
    %Identify regions around the objects (???)
    
    %Identify automatically regions around the objects using CP's function.
    %propagatedImage = IdentifySecPropagateSubfunction(param1_primaryObjectsImageTheObjectsAreIdentifiedWithLabels,param2_TheOriginalImageOnWhichThePropagationWillBeDone,...
    %param3_ThresholdedOriginalImage,param4_regularizationFactorWhichTellsThatTheDividingLinesAreDrawnBasedOnIntensityOfTheSecondaryImageORBasedOnDistance);        
       
    [identifiedRegionLabels,distances] = IdentifySecPropagateSubfunction(CurrentLabelMatrixImage,OrigImage,threshImage,0.05);
    %reduce thresholded boundaries by variance maxima detection
    if strcmp(AreaDetectionThreshold,'Automatic')
        [~,~,identifiedRegionLabels] = plotVarianceChange(identifiedRegionLabels,distances,OrigImage,CurrentLabelMatrixImage,0,sizeLimits(aTrousLevel));
    end            
        
    %TODO: the filtering can be done on the initial variable don't have to
    %copy
    filteredIdentifiedRegionLabels = identifiedRegionLabels;
    %filter out by identified size and circularity
    r = regionprops(identifiedRegionLabels,{'Area','Perimeter','PixelList'});    
    area = cat(1,r.Area);
    perim = cat(1,r.Perimeter);
    maskCell = struct2cell(r);
    maskCell = maskCell(2,:)';
    indicesToDelete = find(area>sizeLimits(aTrousLevel) | area>maxSpotSize | area<minSpotSize);
    %if aTrousLevel>1
        perim(perim == 0) = 1;
        indicesToDelete = union(indicesToDelete,find((area.*4.*pi ./ perim.^2)<circularityThreshold));
    %end
    filteredIdentifiedRegionLabels(ismember(filteredIdentifiedRegionLabels,indicesToDelete)) = 0;
    fileteredArea     =  area(setdiff(1:length(area),indicesToDelete));
    filteredPerimeter = perim(setdiff(1:length(area),indicesToDelete));       
    filteredMaskCell = maskCell(setdiff(1:length(area),indicesToDelete));
    
    identifiedRegionpropsPerLevel{aTrousLevel,1} = filteredIdentifiedRegionLabels;
    identifiedRegionpropsPerLevel{aTrousLevel,2} = fileteredArea;
    identifiedRegionpropsPerLevel{aTrousLevel,3} = filteredPerimeter;
    identifiedRegionpropsPerLevel{aTrousLevel,4} = filteredMaskCell;        
end

%The final label matrix image. First fill it out with the perfect circles.
FinalLabelMatrixImage = zeros(size(OrigImage));
%the entry is -1 if the given object is not coming for the multi-layer
%wavelet transform but from the Hough. Otherwise it is a 2 element vector
%first is the index of the layer the 2nd is theindex of the object
originalID = containers.Map('KeyType','int32','ValueType','any');

%The constantly growing label counter
actualLabel = 1;
finalLabelSet = [];

%
%Detect perfectly circular objects by Hough transform. (Hough transform works reliably only on radii greater than 10.)
radiiRanges = 10:10:(maxSpotRadius-1);
radiiRanges(end+1) = maxSpotRadius;
centers = cell(1,length(radiiRanges));
radii = cell(1,length(radiiRanges));
for i=1:length(radiiRanges)-1
    [centers{i},radii{i}] = imfindcircles(OrigImage,radiiRanges(i:i+1));
    for j=1:size(centers{i},1)
        mask = drawCircle(zeros(size(OrigImage)),centers{i}(j,2),centers{i}(j,1),radii{i}(j),1);
        intensities = OrigImage(logical(mask));
        if mean(intensities) > origMean + origStd * NoiseRemovalFactor
            [FinalLabelMatrixImage,pixelList] = drawCircle(FinalLabelMatrixImage,centers{i}(j,2),centers{i}(j,1),radii{i}(j),actualLabel);
            originalID(actualLabel) = pixelList;
            finalLabelSet = union(finalLabelSet, actualLabel);
            actualLabel = actualLabel + 1;            
        end
    end
end

%after identifying spots on each level remove overlaps from top to bottom.

for i = largestWaveletSize:-1:1 % 2:largestWaveletSize %      
    %labels = unique(identifiedRegionpropsPerLevel{i,1});
    %if labels(1) == 0
    %    labels(1) = [];
    %end    
    for j=1:length(identifiedRegionpropsPerLevel{i,4}) % length(labels) %
        mask = sub2ind(size(identifiedRegionpropsPerLevel{i,1}),identifiedRegionpropsPerLevel{i,4}{j}(:,2),identifiedRegionpropsPerLevel{i,4}{j}(:,1)); % identifiedRegionpropsPerLevel{i,1} == labels(j); %
        %shrinked = imerode(mask,sr);
        %newCirc = circularityByMask(mask,shrinked);
        %if the area is clean yet        
        overlappingRegion = FinalLabelMatrixImage(mask);
        if ~any(overlappingRegion(:))
            FinalLabelMatrixImage(mask) = actualLabel;
            originalID(actualLabel) = [i j];
            finalLabelSet = union(finalLabelSet, actualLabel);
            actualLabel = actualLabel + 1;
        %if there is an overlap      
        elseif i>2 %and we are not on the last level
            newCirc = identifiedRegionpropsPerLevel{i,2}(j)*4*pi/(identifiedRegionpropsPerLevel{i,3}(j)^2);
            overlappingObjectIndices = unique(overlappingRegion);
            if overlappingObjectIndices(1) == 0 %remove 0 if it is there
                overlappingObjectIndices = overlappingObjectIndices(2:end);
            end            
            %circularities = zeros(1,length(overlappingObjectIndices));
            maxCircularity = 0;
            oldMask = []; %zeros(size(FinalLabelMatrixImage));
            for k=1:length(overlappingObjectIndices)
                layerInf = originalID(overlappingObjectIndices(k));
                if numel(layerInf)==2 % if the overlap comes from wavelet detection
                    layer = layerInf(1);
                    subIdx = layerInf(2);
                    actMask = sub2ind(size(OrigImage),identifiedRegionpropsPerLevel{layer,4}{subIdx}(:,2),identifiedRegionpropsPerLevel{layer,4}{subIdx}(:,1)); %FinalLabelMatrixImage == overlappingObjectIndices(k);
                    %r = regionprops(actMask,'Area','Perimeter');               
                    actCircularity = identifiedRegionpropsPerLevel{layer,2}(subIdx)*4*pi/(identifiedRegionpropsPerLevel{layer,3}(subIdx).^2); %r.Area*4*pi/(r.Perimeter.^2);
                else %if the overlap comes from Hough
                    actMask = sub2ind(size(OrigImage),layerInf(:,2),layerInf(:,1));
                    actCircularity = 1; 
                end
                if actCircularity>maxCircularity
                    maxCircularity = actCircularity;
                end
                %circularities(k) = r.Area*4*pi/(r.Perimeter.^2);
                oldMask = union(oldMask,actMask);
            end            
            %if the new object is more circular than the old ones
            if newCirc >  maxCircularity %max(circularities) % mean(circularities) %
                %remove old objects
                FinalLabelMatrixImage(oldMask) = 0;
                finalLabelSet = setdiff(finalLabelSet,overlappingObjectIndices);
                originalID(actualLabel) = [i j];
                FinalLabelMatrixImage(mask) = actualLabel;
                finalLabelSet = union(finalLabelSet, actualLabel);
                actualLabel = actualLabel + 1;
            end
        end
    end
end

%reindex to consecutive indices
FinalLabelMatrixImage = subsetMask(FinalLabelMatrixImage,finalLabelSet);

%Make the outlines.
StructuringElement = strel('square',3);
bitBiggerObjects = imdilate(FinalLabelMatrixImage,StructuringElement);
outlines = bitBiggerObjects - FinalLabelMatrixImage;
LogicalOutlines = outlines > 0;    

% Story END and CP variable savings

if ~isfield(handles.Measurements,SpotObjectName)
    handles.Measurements.(SpotObjectName) = {};
end

%%% Saves the final, segmented label matrix image of secondary objects to
%%% the handles structure so it can be used by subsequent modules.
fieldname = ['Segmented',SpotObjectName];
handles.Pipeline.(fieldname) = FinalLabelMatrixImage;

fieldname = ['SmallRemovedSegmented',SpotObjectName];
handles.Pipeline.(fieldname) = FinalLabelMatrixImage;

handles = CPsaveObjectCount(handles, SpotObjectName, FinalLabelMatrixImage);
handles = CPsaveObjectLocations(handles, SpotObjectName, FinalLabelMatrixImage);

%%% Saves images to the handles structure so they can be saved to the hard
%%% drive, if the user requested.
try
    if ~strcmpi(SaveOutlines,'Do not save')        
        handles.Pipeline.(SaveOutlines) = LogicalOutlines;
    end
catch e
    error(['The object outlines were not calculated by the ', ModuleName, ' module, so these images were not saved to the handles structure. The Save Images module will therefore not function on these images. This is just for your information - image processing is still in progress, but the Save Images module will fail if you attempted to save these images.'])
end

toc


%A_trous transform
function w = a_trous(in, level,filterSize)

in = double(in);

[sizex sizey] = size(in);

c = zeros(sizex, sizey, level);

w = zeros(sizex, sizey, level);

c(:, :, 1) = in;

for i=2:level

    c(:, :, i) = calculateC(in, i-2,filterSize);

    w(:,:,i-1) = c(:, :, i) - c(:, :, i-1);

end;

w(:,:,i) = c(:,:,i);


function out = calculateC(in, level,filterSize)

[sizex sizey] = size(in);

rlevel = 2^level;

if sizex < 2 * rlevel || sizey < 2 * rlevel
    
    disp('Too huge level value');
    
    return;
    
end;

%% boundary mirroring
inb = zeros(sizex + 4*rlevel, sizey + 4*rlevel);
inb(2*rlevel+1:2*rlevel+sizex, 2*rlevel+1:2*rlevel+sizey) = in;

inb(2*rlevel+1:2*rlevel+sizex, 1:2*rlevel) = in(1:sizex, 2*rlevel:-1:1);
inb(2*rlevel+1:2*rlevel+sizex, 2*rlevel+sizey+1:sizey+4*rlevel) = in(1:sizex, sizey:-1:sizey-2*rlevel+1);

inb(1:2*rlevel, 1:sizey + 4*rlevel) = inb(4*rlevel:-1:2*rlevel+1, 1:sizey + 4*rlevel);
inb(2*rlevel+sizex+1:sizex+4*rlevel, 1:sizey + 4*rlevel) = inb(2*rlevel+sizex:-1:sizex+1, 1:sizey + 4*rlevel);
filter = createfilter_2d(1, filterSize, level^2);
%Why the convolution is done on the in image? Or at least then why the inb
%computed at all?
out = conv2(double(inb),filter,'same');

% cut borders

cutsize = (size(filter, 1)-1) / 2;

out = out(cutsize+1:cutsize+sizex, cutsize+1:cutsize+sizey);


function filt = createfilter_2d(sigma, size, gap)

if nargin == 2
    gap = 0;
end;

filt = zeros((size-1)*(gap+1)+1, (size-1)*(gap+1)+1);

for i=1:size
    for j=1:size
        middle = size/2;
        dist = sqrt((middle-i+0.5)^2+(middle-j+0.5)^2);
        filt((i-1)*(gap+1)+1, (j-1)*(gap+1)+1) = normpdf(dist, 0, sigma);
    end;
end;

filt = filt ./ sum(filt(:));

%thresold the image according to a percentile to remove from the low
%intensity region
function threshImage = thresholdPercentile(image,percent)
    [~,idx] = sort(image(:));
    threshold = image(idx(round(numel(image)*percent)));
    threshImage = image > threshold;
    

function newMean = onlineMean(prevMean,xn,n)    
    newMean = prevMean + (xn-prevMean)/n;
    
function [newM,xnA] = onlineDiffFromMeanSquared(prevM,prevMean,xn,n)
    xnA = onlineMean(prevMean,xn,n);
    newM = prevM + (xn-prevMean)*(xn-xnA);    

 function [circularity,shrinked] = circularityByMask(mask,shrinked,area)
%Circularity is measured by area*4*pi / perimeter.^2. Theoretically this
%value is 1 for the circle and smaller for all other graphical object but
%in practice it can go above 1.
%INPUT:
%   mask        A binary image with the object
%   area        The area of the object can be given to fasten the
%               calculation but it is optional.
%   shirnked    A shrinked version of the mask, for calculating perimeter.
%   
%OUTPUT:
%   circularity The circularity value of the binary object in mask.

if nargin<3
    area = sum(mask(:));
end

contours = mask-shrinked;
contourChain = find(contours);
perimeter = length(contourChain);
circularity = area*4*pi / (perimeter.^2);

function [img,idx] = drawCircle(img,x,y,r,numberToFill)
%fills up with one if the last parameter is not specified
%in case of index out of bound it just jumps.
%returns the image and the index positions like regionprops.

if nargin<5
    numberToFill = 1;
end

alpha = 0:0.01:2*pi;
rrange = 0:r/100:r;
idx = zeros(length(alpha)*length(rrange),2);
currPos = 1;
for i=1:length(alpha)
    for j = rrange        
        if (round(x+j*cos(alpha(i))) <= size(img,1) && round(x+j*cos(alpha(i)))>0  && round(y+j*sin(alpha(i)))<=size(img,2) && round(y+j*sin(alpha(i)))>0)            
            idx(currPos,:) = [round(y+j*sin(alpha(i))), round(x+j*cos(alpha(i)))];
            img(idx(currPos,2),idx(currPos,1)) = numberToFill;
            currPos = currPos + 1;             
        end
    end
end
idx(currPos:end,:) = []; %delete remaining elements

 function currentMask = subsetMask(mask, currentGroup)           
 %Makes a subset mask out of a given mask. (reindex with consecutive
 %indices).
 %  INPUTS:
 %      mask            An input mask image
 %      currentGroup    Subset of the mask indices which should be kept in
 %                      the subset mask.
    currentMask = zeros(size(mask));
    r = regionprops(mask,'PixelIdx');
    for i=1:length(currentGroup)                       
        currentMask(r(currentGroup(i)).PixelIdxList) = i;
    end
    
function [vari,img,newRegionLabels] = plotVarianceChange(identifiedRegionLabels,distances,intensityImage,centerImage,toPlot,maxArea)
%script to plot out variance change inside the identified particles
    
    %for all identified subregion
    sorted = sort(centerImage(:));
    positives = sorted(sorted>0);
    vari = cell(1,length(positives));
    k = 1;
    img = zeros(size(intensityImage));
    newRegionLabels = zeros(size(intensityImage));
    for i=positives'
        origIdx = find(identifiedRegionLabels == i);
        dist = distances(origIdx);
        inten = intensityImage(origIdx);
        [~,sortedIdx] = sort(dist);
        vari{k} = zeros(1,length(sortedIdx));
        
        actMean = inten(sortedIdx(1));
        soFarDiv = 0;
        vari{k}(1) = 0;
        for j=2:length(vari{k})
            [soFarDiv,actMean] = onlineDiffFromMeanSquared(soFarDiv,actMean,inten(sortedIdx(j)),j);
            vari{k}(j) = soFarDiv / j;
            img(origIdx(sortedIdx(j))) = vari{k}(j);
        end
        
        %find the first local maxima
        %maxIndices = localMaxima(vari{k},50);
        %if isempty(maxIndices)
        %    maxIndices = length(vari{k});
        %end
        [~,maxIndices] = max(vari{k});
        if nargin>5
            endPoint = min(maxArea,maxIndices(1));
            newRegionLabels(origIdx(sortedIdx(1:endPoint))) = i;
        else
            newRegionLabels(origIdx(sortedIdx(1:maxIndices(1)))) = i;
        end
        
        %plot if requested
        if toPlot
            f = figure();
            mask = identifiedRegionLabels == i;
            imshow(mask);
            g = figure();
            plot(vari{k});
            s = input('Do you want to save this figure? [y/n]','s');
            if s=='y'
                saveas(gcf,['variPlot_' num2str(i,'%03d') '.png']);
            elseif s=='s'
                break;
            end
            close(g);
            close(f);
        end
        k = k+1;
    end
    
    
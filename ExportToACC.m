function handles = ExportToACC(handles)

% Save data into acc format
% Category: File Processing
%
% SHORT DESCRIPTION:
% Save the measurements into text files can be used by Advanced Cell
% Classifier
% *************************************************************************
%
% In ACC one can train Machine Learning algorithms to classify cells
% (feature vectors). The cells corresponds to a primary object (usually
% nuclei) which should be (for correct working) the first selected object
% to export in this module. The other objects selected to export must be
% related to this primary object either by implicit relation (each
% secondary object is implicitely related to primary objects) or by
% explicitly calling the relate module.
%
% In order to export objects to ACC which are not in 1-by-1
% relation with the primary object (e.g. usually nuclei is the primary
% object and to each cytoplasm there is only one nuclei), you MUST RUN A
% RELATE MODULE on the 'multiple' objects. Multiple here means that the
% object is NOT in 1-by-1 relation with the primary object but there are
% more of them. (e.g. each cell can have more than one subcellular
% particles such as lipid droplets). These objects must be signed as
% 'multiple' object providing the information that we should use the MEAN
% measurements for them. (In this case in each cell describing feature
% vector there will be entries which contains mean measurements from the
% multiple objects, e.g. the average area of lipid droplets within one
% cell).
%
% In order to use your project in ACC your saved images (such as the
% original coloured image, or the image with the contours) MUST HAVE THE
% SAME NAME. (e.g. you should select the same original channel name for
% both of them in the SaveImages module). In this module you should select
% the same channel as the base for the export to ACC.
%
% In this module we assumed that the similar channel images are coming from
% a similar channel folder.
%
% For correct working before running this module DELETE the ECP.mat file
% from the folder specified as Default Output folder.
%
% CellProfiler is distributed under the GNU General Public License.
% See the accompanying file LICENSE for details.
%
% Developed by Peter Horvath.
% Copyright 2010.
%
% Please see the AUTHORS file for credits.
%
% Website: http://acc.ethz.ch
%
% $Revision: 5025 $

%%%%%%%%%%%%%%%%%
%%% VARIABLES %%%
%%%%%%%%%%%%%%%%%
drawnow

[~, CurrentModuleNum, ~] = CPwhichmodule(handles);


%textVAR01 = Where do you want to save the measurements
%defaultVAR01 = anal2
OriginalFolder = char(handles.Settings.VariableValues{CurrentModuleNum,1});

%textVAR02 = Which images' original filenames do you want use as a base for the export? To make ACC work correctly you should set this to the same original filename with which you saved your images.
%infotypeVAR02 = imagegroup
CPInternalACCImageID = char(handles.Settings.VariableValues{CurrentModuleNum,2});
%inputtypeVAR02 = popupmenu custom

%textVAR03 = Which objects do you want to export? Cell location will be calculted from the first object. Normally use here nuclei. (Object #1 - PRIMARY)
%infotypeVAR03 = objectgroup
%choiceVAR03 = /
%choiceVAR03 = Image
%choiceVAR03 = Experiment
Object{1} = char(handles.Settings.VariableValues{CurrentModuleNum,3});
%inputtypeVAR03 = popupmenu

MultipleItem{1} = 'No';

%textVAR04 = Object #2
%infotypeVAR04 = objectgroup
%choiceVAR04 = /
%choiceVAR04 = Image
%choiceVAR04 = Experiment
Object{2} = char(handles.Settings.VariableValues{CurrentModuleNum,4});
%inputtypeVAR04 = popupmenu

%textVAR05 = Is this a multiple object?
%choiceVAR05 = No
%choiceVAR05 = Yes
MultipleItem{2} = char(handles.Settings.VariableValues{CurrentModuleNum,5});
%inputtypeVAR05 = popupmenu


%textVAR06 = Object #3
%infotypeVAR06 = objectgroup
%choiceVAR06 = /
%choiceVAR06 = Image
%choiceVAR06 = Experiment
Object{3} = char(handles.Settings.VariableValues{CurrentModuleNum,6});
%inputtypeVAR06 = popupmenu

%textVAR07 = Is this a multiple object?
%choiceVAR07 = No
%choiceVAR07 = Yes
MultipleItem{3} = char(handles.Settings.VariableValues{CurrentModuleNum,7});
%inputtypeVAR07 = popupmenu


%textVAR08 = Object #4
%infotypeVAR08 = objectgroup
%choiceVAR08 = /
%choiceVAR08 = Image
%choiceVAR08 = Experiment
Object{4} = char(handles.Settings.VariableValues{CurrentModuleNum,8});
%inputtypeVAR08 = popupmenu

%textVAR09 = Is this a multiple object?
%choiceVAR09 = No
%choiceVAR09 = Yes
MultipleItem{4} = char(handles.Settings.VariableValues{CurrentModuleNum,9});
%inputtypeVAR09 = popupmenu

%textVAR10 = Do you want to export for Object analyser?
%choiceVAR10 = Yes
%choiceVAR10 = No
ObjectAnalyserText = char(handles.Settings.VariableValues{CurrentModuleNum,10});
%inputtypeVAR10 = popupmenu

%pathnametextVAR11 = (Only for export to Object analyser) Specify the folder where you whish to save your mask images! (Similar to save images module but NOT the same. Type dot (.) for the default output folder and ampersand (&) the default (preferences) output folder. 
FileDirectory = char(handles.Settings.VariableValues{CurrentModuleNum,11});


%%%VariableRevisionNumber = 2

a = tic;

drawnow
tmp = {};
tmpMul = {};
for n = 1:4
    if ~strcmp(Object{n}, '/')
        tmp{end+1} = Object{n}; %#ok<AGROW> at most object number of operation. Currently 4.
        tmpMul{end+1} = MultipleItem{n}; %#ok<AGROW> at most object number of operation. Currently 4.
    end
end
Object = tmp;
MultipleItem = tmpMul;

if strcmp(ObjectAnalyserText,'Yes')
    objectAnalyserBool = 1;
else
    objectAnalyserBool = 0;
end

%Process the file directory string for mask images
if objectAnalyserBool
    if strncmp(FileDirectory,'.',1)
        PathName = handles.Current.DefaultOutputDirectory;        
        FileDirectory = FileDirectory(2:end);                            
    elseif strncmp(FileDirectory, '&', 1)    
        PathName = handles.Preferences.DefaultOutputDirectory;            
        FileDirectory = FileDirectory(2:end);      
    else
        PathName = FileDirectory;
        FileDirectory = '';
    end
end

accIndex = 0;
%Identify the index of the ACCImageName base channel.
for j=1:length(handles.Measurements.Image.PathNamesText)
    if strcmp(handles.Measurements.Image.PathNamesText{j},CPInternalACCImageID)
        accIndex = j;
        break;
    end
end


if accIndex == 0
    error(['Image processing was canceled in the ', ModuleName, ' module because the specified image (',CPInternalACCImageID,') could not be found in the original image list.']);
end

% last cycle
%if handles.Current.SetBeingAnalyzed == handles.Current.NumberOfImageSets
% in the new version we do it for every cycle
if 1
    % operating system specific slash
    if ispc == 1
        OSSlash = '\';
    else
        OSSlash = '/';
    end;

    % output folder
    outPutFolder = handles.Current.DefaultOutputDirectory;
    if outPutFolder(length(outPutFolder)) ~= OSSlash
        outPutFolder = [outPutFolder OSSlash];
    end;
    
    %Check for duplicate entry
    [ObjectNames,indices] = unique(Object);
    MultipleItem = MultipleItem(indices);
        
    % for each image sets
    % Note: position of the objects comes from the first measured object
    % type (usually nuclei)
    
    %for i=1:handles.Current.NumberOfImageSets
    % current cycle
    i = handles.Current.SetBeingAnalyzed;    
     
        %Export to Object analyzer and to ECP format (startup)
        if objectAnalyserBool
            if ~exist(fullfile(outPutFolder,'ECP.mat'),'file')
                ECP.RawImages.ChannelName = getCPchannels(handles);
                ECP.RawImages.Directory = handles.Measurements.Image.PathNames{1};
                ECP.RawImages.CPNames = handles.Measurements.Image.PathNamesText;
                ECP.PrimaryChannel = ECP.RawImages.ChannelName{accIndex};                
                ECP.PrimaryObject = Object{1};
                ECP.Images = cell(0);
                ECP.DirName = PathName; %This should be equivalent with the ACC DirName after a while
            else
                load(fullfile(outPutFolder,'ECP.mat'));               
            end                        
        end
        
        ACCImageName = char(handles.Measurements.Image.FileNames{i}(accIndex));
        outputFileName = [ACCImageName '.txt'];
    
        wdata = [];        
        
        % get xy positions
        PosField = handles.Measurements.(Object{1}); %here we should use the old Object because ObjectNames is sorted.
        wdata(:,1:2) = PosField.Location{i}(:,1:2); % nucleii location       
        header = {};
        header{1} = 'LocationX';
        header{2} = 'LocationY';
        featureCounter = 2;
        
        % get measured fileds:        
        for j=1:length(ObjectNames)
            
            %ECP export parts
            if objectAnalyserBool 
                currentObjectMaskFolder = fullfile(PathName,FileDirectory,ObjectNames{j});
                if ~isfieldRecursive(ECP,['Objects.' ObjectNames{j} '.FolderName']);                    
                    ECP.Objects.(ObjectNames{j}).FolderName = fullfile(FileDirectory,ObjectNames{j});
                end                
                if ~exist(currentObjectMaskFolder,'dir')
                    mkdir(currentObjectMaskFolder);
                end
                maskImage = handles.Pipeline.(['Segmented', ObjectNames{j}]);
                saveMaskImage(maskImage,currentObjectMaskFolder,ACCImageName);                
            end
            
            % get measurements
            % if this is a simple object then just the ObjectNames
            if strcmp(MultipleItem{j},'No')          
               measurementField = handles.Measurements.(ObjectNames{j}); 
            else %otherwise we suppose there are mean measurements.
               measurementField = handles.Measurements.(['Mean' ObjectNames{j}]);               
            end            
            
            %%
            
            [locWData,locHeader,~] = transformMeasurementField(measurementField,{'Location','Parent','Children'},i,ObjectNames{j});
            wdata = [wdata locWData]; %#ok<AGROW> Only few (object number) times
            header = [header locHeader]; %#ok<AGROW> Only few times
            
            %%
            
            %Export to Object analyser
            if objectAnalyserBool                
                measurementField = handles.Measurements.(ObjectNames{j}); 
                [~,~,structure] = transformMeasurementField(measurementField,{'Location','Parent','Children'},i,ObjectNames{j});
                ECP.Images{i}.(ObjectNames{j}).Measurements = structure;
                %For the primary object we might not have Parent field. However for all others we must have. This is NOT checked here               
                if isfield(measurementField,'Parent')
                    ECP.Images{i}.(ObjectNames{j}).Parent = measurementField.Parent{i};
                end                
                [~,ACCImageNameExcExt,~] = fileparts(ACCImageName);
                ECP.Images{i}.ImageName = ACCImageNameExcExt;
            end
                        
        end;
        % save output
        save([outPutFolder OriginalFolder OSSlash outputFileName], 'wdata', '-ascii');
        % save feature description for further analisys
        if ~exist([outPutFolder OriginalFolder OSSlash 'featureNames.acc'],'file')
            outFile = fopen([outPutFolder OriginalFolder OSSlash 'featureNames.acc'], 'w');
            for l=1:length(header);
                fprintf(outFile, '%s\n', header{l});
            end;
            fclose(outFile);
        end;                       
    
        
    if objectAnalyserBool
        save(fullfile(outPutFolder,'ECP.mat'),'ECP');
    end
    %end;    
end

toc(a);

%to extract typed in channel names
function channelArray =  getCPchannels(handles)
channelArray = cell(0);
for i=1:length(handles.Settings.ModuleNames)    
    if strcmp(handles.Settings.ModuleNames(i),'LoadImages')
        j = 2;
        while (~strcmp(handles.Settings.VariableValues{i,j},'/') && j<10)
            channelArray{end+1} = handles.Settings.VariableValues{i,j}; %#ok<AGROW>Size can't be estimated before, but not many usually.
            j = j + 2;            
        end
    end
end

%Saves the maskImage to the folder. If the mask image has multiple layers
%(i.e. its 3rd dimension is greater than 1) then it saves multiple images 
function saveMaskImage(maskImage,folder,imageName)
%single layer
if size(maskImage,3)==1
    imwrite(uint16(maskImage),fullfile(folder,imageName));
else
    [~,base,~] = fileparts(imageName);
    for i=1:size(maskImage,3)
        imwrite(uint16(maskImage(:,:,i)),fullfile(folder,[base sprintf('_l%02d',i) '.tif']));
    end        
end

function [wdata,header,structure] = transformMeasurementField(measurementField,omitFields,currentSet,objectName)
%transformMeasurementField
% The purpose of this function is to help in the process of
% transforming the CP measurement fields to ACC feature vectors. CP makes
% measurements for each objects and stores them in
% handles.Measurements.(ObjectName).xxx fields. xxx can end to 'Features'
% in which case that field stores the interpretation of the columns in the
% next field.
% In one run this function extracts features corresponding to one image
% this should be specified in the currentSet input. (This value identifies
% which cell will be used from the xxx cellarray)
%
%   INPUTS:
%       measurementField        the CP measurement field
%       omitFields              measurement fields containing any of the
%                               strings listed in this cellarray will be
%                               excluded from the output.
%       currentSet              Which image to use.
%       objectName              This will be included as a prefix in
%                               headers.
%       
%   OUTPUT:
%       wdata                   Matrix based output for regular ACC use
%       header                 Feature names of wdata in a cellarray.
%       structure               The restructured form of CP for ACC-OA.

measurementFieldNames = fieldnames(measurementField);
ftext = 'Features';
measurementFieldNamesDescrPos = strfind(measurementFieldNames, ftext);

% avoid the omit fields
indicators = zeros(length(measurementFieldNames),length(omitFields));

for i=1:length(omitFields)
    for k=1:length(measurementFieldNames)
        indicators(k,i) = isempty(strfind(measurementFieldNames{k}, omitFields{i}));
    end
end

header = {};
wdata = [];
structure = [];

for k=find(all(indicators'))
    %if this IS a Feature descibing position
    if ~isempty(measurementFieldNamesDescrPos{k})
        % put feature names
        featureNameList = measurementField.(measurementFieldNames{k});
        %split up according to underscores the part before the Features
        measType = measurementFieldNames{k}(1:measurementFieldNamesDescrPos{k}(1)-1);        
        % put data
        for l=1:length(featureNameList)
            header = [header [objectName '.' measurementFieldNames{k} '.' featureNameList{l}]]; %#ok<AGROW> not much ~3-400 features max.
        end
        featureValues = measurementField.(measurementFieldNames{k+1});
        currentData = featureValues{currentSet};
        %TODO: compute the number of non omitted feature beforehand
        wdata = [wdata (currentData)];  %#ok<AGROW> It is much hassle but can be done.
        structure.(measType).Data = currentData; %store the data
        structure.(measType).Version = 0; %native CP is version 0.
    end
end

%CUSTOM EXTRA FUNCTIONS from ACC
function fieldInDeep = isfieldRecursive(CommonHandles,string) %#ok<INUSL> it is used in eval.
% AUTHOR:   Abel Szkalisity
% DATE:     August 16, 2016
% NAME:     isfieldRecursive
%
% Function to check fields recursively.
%
% INPUT:
%       CommonHandles  usual CommonHandles
%       string         A string which will be parsed by the dots
%
% OUTPUT:
%       fieldInDeep    Boolean value, true iff the field described by
%                      string is really field of CommonHandles
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.


splittedString = strsplit(string,'.');
fieldInDeep = 1;

for i=1:length(splittedString)
    if i>1
        baseString = 'CommonHandles.';
    else
        baseString = 'CommonHandles';
    end
    if eval(['~isfield(' baseString strjoin(splittedString(1:i-1),'.') ', ''' splittedString{i} ''')'])
        fieldInDeep = 0;
        break;
    end
end

%Implementation of strsplit and strjoin
function joinedString = strjoin(inputStringCell,borderString)
    if isempty(inputStringCell)
        joinedString = [];
        return;
    end
    
    joinedString = '';
    for i=1:length(inputStringCell)-1
        joinedString = [joinedString inputStringCell{i} borderString];
    end
    joinedString = [joinedString inputStringCell{end}];    
    
    
function splittedCellArray = strsplit(inputStr,delimStr)
    pos = strfind(inputStr,delimStr);
    if ~isempty(pos)
        splittedCellArray = cell(1,length(pos)+1);
        splittedCellArray{1} = inputStr(1:pos(1)-1);
        for i=2:length(pos)
            splittedCellArray{i} = inputStr(pos(i-1)+1:pos(i)-1);
        end
        splittedCellArray{length(pos)+1} = inputStr(pos(i)+1:end);
    else
        splittedCellArray = {inputStr};
    end



    
    